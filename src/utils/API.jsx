import axios from 'axios'

const HasRootProxy = axios.create({
  baseURL: 'https://kobi.dev/nopixel'
})

const GetStreams = () => {
  return HasRootProxy.get('hasroot_list.php')
}

export { GetStreams }
