import styled from "styled-components";

const StreamGridContainer = styled.div`
  padding-top: ${props => (props.watching ? 0 : 65)}px;
`;

export { StreamGridContainer };
