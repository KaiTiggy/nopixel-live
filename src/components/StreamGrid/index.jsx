import React from 'react'
import { Row } from 'reactstrap'

import StreamLink from '../StreamLink'
import { StreamGridContainer } from './styles'

const StreamGrid = props => {
  return (
    <StreamGridContainer {...props}>
      <Row noGutters>
        {props.streams
          .map(stream =>
            <StreamLink
              {...stream}
              preview={`https://static-cdn.jtvnw.net/previews-ttv/live_user_${stream.name}-320x180.jpg`}
              onClick={() => props.parent.setState({ watching: stream })}
              active={
                props.watching &&
                props.watching.channel_id === stream.channel_id
              }
            />)}
      </Row>
    </StreamGridContainer>
  )
}

export default StreamGrid
