import styled from 'styled-components'

const FooterText = styled.div`
  font-size: 0.8em;
  color: #fefefe;
`

export { FooterText }
