import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircle } from '@fortawesome/pro-regular-svg-icons'

import { FooterText } from './styles'

const Footer = () => {
  return (
    <FooterText className='mt-3 mb-2 text-center'>
      <div>
        NoPixel.live is <strong>not</strong> affiliated with or endorsed by
        NoPixel.
      </div>
      <div>
        Data provided by&nbsp;
        <a href='https://nopixel.hasroot.com'>nopixel.hasroot.com</a>,&nbsp;
        not affiliated with or endorsed by HasRoot.
      </div>
      <div className='mt-1'>
        Created by <a href='https://kobitate.com'>Kobi Tate</a>
        &nbsp;
        <FontAwesomeIcon
          size='sm'
          fixedWidth
          icon={faCircle}
          className='mx-1'
        />
        &nbsp;
        <a href='https://gitlab.com/KaiTiggy/nopixel-live'>View Source Code</a>
      </div>
    </FooterText>
  )
}

export default Footer
