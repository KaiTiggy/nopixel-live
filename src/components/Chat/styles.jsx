import styled from "styled-components";

const ChatContainer = styled.div`
  padding-top: 50px;
  background: #0f0d11;
`;

const ChatFrame = styled.iframe`
  width: 100%;
  height: ${props => props.playerHeight + 9}px;
  border: none;
`;

export { ChatContainer, ChatFrame };
