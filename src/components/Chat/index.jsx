import React from 'react'
import { ChatContainer, ChatFrame } from './styles'

const Chat = props =>
  props.watching ? (
    <ChatContainer>
      <ChatFrame
        {...props}
        src={`https://www.twitch.tv/embed/${
          props.watching.name
        }/chat?darkpopout`}
        id={props.watching.name}
      />
    </ChatContainer>
  ) : null

export default Chat
