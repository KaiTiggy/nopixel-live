import styled from "styled-components";

const Handle = styled.div`
  margin: 5px auto;
  z-index: 9999;
  width: 50px;
  height: 5px;
  transition: all 0.3s ease-out;
  background: rgba(255, 255, 255, 0.5);
  border-radius: 10px;
  &:hover {
    background: rgba(255, 255, 255, 0.7);
  }
  ${props => props.dragging && 'background: rgba(255, 255, 255, 1);'};

`;

export { Handle };
