import React from 'react'
import { Handle } from './styles'

const ResizeHandle = props => <Handle {...props} />

export default ResizeHandle
