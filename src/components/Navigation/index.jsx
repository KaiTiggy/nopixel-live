import React from 'react'
import { Nav, NavItem, NavLink, Badge } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faThumbtack as pinSolid,
  faSyncAlt,
  faCommentAlt as commentSolid,
  faCircle
} from '@fortawesome/pro-solid-svg-icons'
import {
  faThumbtack as pinRegular,
  faCommentAlt as commentRegular
} from '@fortawesome/pro-regular-svg-icons'

import { Navbar, NavbarBrand, FlashingIcon } from './styles'

const Navigation = props => {
  const streamsOnline = Object.entries(props.streams).length
  return (
    <Navbar fixed='top' expand='xs'>
      <NavbarBrand>
        NoPixel Live
        <Badge color='dark' pill className='ml-2'>
          {props.loading ? (
            'Loading...'
          ) : (
            props.streams && <React.Fragment>
              <FlashingIcon className='text-danger' icon={faCircle} />
              {streamsOnline}&nbsp;
              <span className='d-none d-sm-inline'>
                {streamsOnline === 1
                  ? 'channel'
                  : 'channels'
                }
                &nbsp;
              </span>
              online
            </React.Fragment>
          )}
        </Badge>
      </NavbarBrand>
      <Nav navbar className='ml-auto'>
        <NavItem className='ml-3'>
          <NavLink onClick={props.reload}>
            <FontAwesomeIcon
              icon={faSyncAlt}
              className={props.loading ? 'text-success' : ''}
              spin={props.loading}
            />
          </NavLink>
        </NavItem>
        <NavItem className='ml-3'>
          <NavLink onClick={props.toggleChat}>
            <FontAwesomeIcon
              icon={props.chat ? commentSolid : commentRegular}
            />
          </NavLink>
        </NavItem>
        <NavItem className='ml-3'>
          <NavLink onClick={props.togglePinned}>
            <FontAwesomeIcon
              icon={props.playerPinned ? pinSolid : pinRegular}
            />
          </NavLink>
        </NavItem>
      </Nav>
    </Navbar>
  )
}

export default Navigation
