import styled, { keyframes } from "styled-components";
import { Navbar as N, NavbarBrand as NB } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const flash = keyframes`
  0% {
    opacity: 0;
  }

  25% {
    opacity: 1;
  }

  75% {
    opacity: 1;
  }

  100% {
    opacity: 0;
  }
`;

const Navbar = styled(N)`
  background: linear-gradient(
    to bottom,
    rgba(0, 0, 0, 1) 0%,
    rgba(0, 0, 0, 1) 1%,
    rgba(0, 0, 0, 0.9) 23%,
    rgba(0, 0, 0, 0) 100%
  );
  color: white !important;
  z-index: 10000 !important;
  a {
    cursor: pointer;
  }
`;

const NavbarBrand = styled(NB)`
  color: white !important;
`;

const FlashingIcon = styled(FontAwesomeIcon)`
  animation: ${flash} 1.5s ease-in-out infinite reverse;
  font-size: 0.9em;
  margin-right: 5px;
`;

export { Navbar, NavbarBrand, FlashingIcon };
