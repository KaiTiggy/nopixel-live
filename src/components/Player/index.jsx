import React from 'react'
import ReactPlayer from 'react-player'
import { PlayerContainer } from './styles'

const Player = props => {
  return (
    <PlayerContainer {...props}>
      <ReactPlayer
        url={`https://twitch.tv/${props.channel}`}
        width='100%'
        height={props.height}
        playing
      />
    </PlayerContainer>
  )
}

export default Player
