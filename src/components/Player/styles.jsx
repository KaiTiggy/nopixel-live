import styled from "styled-components";

const PlayerContainer = styled.div`
  padding-top: 50px;
  padding-bottom: 15px;
  background: black;
  z-index: 9998;
  position: ${props => (props.pinned ? "fixed" : "static")};
`;

export { PlayerContainer };
