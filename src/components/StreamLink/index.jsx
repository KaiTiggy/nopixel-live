import React from 'react'
import { Col, Card, CardImg, Badge } from 'reactstrap'
import { ChannelInfo, Name, Title, PlayIcon } from './styles'
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome'
import { faPlay, faEye } from '@fortawesome/pro-solid-svg-icons'

const StreamLink = props => {
  return (
    <Col xs={12} sm={6} lg={3}>
      <Card>
        <CardImg top width='100%' src={props.preview} onClick={props.onClick} />
        <ChannelInfo onClick={props.onClick}>
          <Name>{props.displayName}</Name>
          <Title>
            {props.status}
            <Badge pill color='secondary' className='ml-1'>
              <Icon icon={faEye} />&nbsp;
              {props.viewers}
            </Badge>
          </Title>
        </ChannelInfo>
      </Card>
      {props.active && <PlayIcon icon={faPlay} />}
    </Col>
  )
}

export default StreamLink
