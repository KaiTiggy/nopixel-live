import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const ChannelInfo = styled.div`
  background: rgba(12, 22, 34, 0.7);
  width: 100%;
  color: white;
  position: absolute;
  left: 0;
  bottom: 0;
  padding: 10px;
  text-align: center;
  backdrop-filter: blur(3px) brightness(50%) saturate(150%);
`

const Name = styled.div`
  font-weight: bold;
`

const Title = styled.div`
  font-size: 0.8em;
`

const PlayIcon = styled(FontAwesomeIcon)`
  position: absolute;
  right: 0;
  top: 0;
  color: #00c81e;
  margin: 10px;
  font-size: 1.8em;
  filter: drop-shadow(3px 3px 2px rgba(0, 0, 0, 0.7));
`

export { ChannelInfo, Name, Title, PlayIcon }
