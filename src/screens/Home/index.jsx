import React from 'react'
import Navigation from '../../components/Navigation'
import { Container, Row, Col } from 'reactstrap'
import Sticky from 'react-sticky-el'

import { GetStreams } from '../../utils/API'

import StreamGrid from '../../components/StreamGrid'
import Player from '../../components/Player'
import ResizeHandle from '../../components/ResizeHandle'
import Footer from '../../components/Footer'
import Chat from '../../components/Chat'
import Draggable from 'react-draggable'

export default class Home extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      streams: [],
      watching: null,
      loading: false,
      playerHeight: 500,
      playerPinned: true,
      chat: true,
      dragging: false
    }
  }

  componentDidMount () {
    this.searchStreams()
    setInterval(() => this.searchStreams(), 300000)
  }

  searchStreams () {
    this.setState({ loading: true })
    GetStreams().then(data => {
      let streams = []
      for (const [key, stream] of Object.entries(data.data.streams)) {
        console.log(key, stream)
        streams.push(stream)
      }
      streams = streams
        .filter(stream => stream.serverGroup === 1 && stream.online === 1)
        .sort((streamA, streamB) => streamA.viewers - streamB.viewers)
        .reverse()
      this.setState({ streams, loading: false })
    })
  }

  render () {
    return (
      <Container fluid>
        <Row>
          <Col>
            <Navigation
              {...this.state}
              togglePinned={() =>
                this.setState({ playerPinned: !this.state.playerPinned })
              }
              toggleChat={() => this.setState({ chat: !this.state.chat })}
              reload={() => this.searchStreams()}
            />
          </Col>
        </Row>
        {this.state.watching && (
          <Sticky style={{ zIndex: 9998 }} disabled={!this.state.playerPinned}>
            <Row noGutters>
              <Col>
                <Player
                  channel={this.state.watching.name}
                  height={this.state.playerHeight}
                />
              </Col>
              {this.state.chat && (
                <Col xs={12} sm={3}>
                  <Chat {...this.state} />
                </Col>
              )}
            </Row>
            <Row noGutters>
              <Col xs={12}>
                <Draggable
                  axis='y'
                  onStart={() => this.setState({ dragging: true })}
                  onDrag={e => {
                    this.setState({ playerHeight: e.clientY - 57 })
                  }}
                  onStop={() => this.setState({ dragging: false })}>
                  <ResizeHandle
                    {...this.state}
                  />
                </Draggable>
              </Col>
            </Row>
          </Sticky>
        )}
        <StreamGrid {...this.state} parent={this} />
        <Row>
          <Col>
            <Footer />
          </Col>
        </Row>
      </Container>
    )
  }
}
