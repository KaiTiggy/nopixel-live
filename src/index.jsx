import React from 'react'
import ReactDOM from 'react-dom'
import Router from './utils/Router'
import './index.css'
import 'bootstrap/dist/css/bootstrap.css'
// import "bootswatch/dist/pulse/bootstrap.css";

ReactDOM.render(<Router />, document.getElementById('root'))
