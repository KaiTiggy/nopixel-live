# [NoPixel.live](https://nopixel.live)

Experience the quickest and easiest way to jump between streamers' channels on the NoPixel GTA Roleplay server. This site reads the unofficial official list of streamers online from [NoPixel Hasroot](https://nopixel.hasroot.com), and presents them in an easy-to-use grid, allowing visitors to jump between streamers in a flash.

![Preview](https://i.imgur.com/01kAhrB.jpg)

<sup>Note: This website and repository are <strong>not</strong> affiliated with NoPixel.net or nopixel.hasroot.com</sup>
